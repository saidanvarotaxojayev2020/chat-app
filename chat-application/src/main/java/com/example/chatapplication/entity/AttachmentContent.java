package com.example.chatapplication.entity;

import com.example.chatapplication.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AttachmentContent extends AbsEntity {

    @OneToOne(fetch = FetchType.LAZY,optional = false,cascade = CascadeType.ALL)
    private Attachment attachment;

    @Column(nullable = false)
    private byte[] bytes;


}
